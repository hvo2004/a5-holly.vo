The app contains 3 activities and 1 remote service
- Remote service handle online look up meaning and thesaurus of a word and store in local database per user request.
	- for lookup, service looks at local DB and only go online to search if it's not in DB (only 1000 online api calls can be made a day, so please store local for saving api calls & for reviewing purpose.)
	- if you look up a simple English word, please do not suprise that it has no thesaurus.
- 3 activities: 
	- lookup (in action bar)
		- each term usually is associated with multiple meanings, use "previous" and "next" icon button at the end to see all
		- if the term is retrieved only, "+ Local Book" is available
		- if the term is retrieved from local book, "x Local Book" is available to remove the record and its associated meanings from local DB.
	- local book (in action bar)
		- list all terms stored locally and provide access to review meanings of each term by selection
	- review mode: accessed through local book only
		- mimic the index memory card with 2 sides: term on 1 side and meanings on the other side for flipping
		- switch button to flip "index card"
- Tested Devices: Samsung A4/A5
- The project is designed for other purposes beyond learning the new word and its thesaurus, such as provide quiz and score statistics (as proposed but do not have enough time to implement all).

	
		
package x40241.holly.vo.a5.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 
 * @author hvo
 * Meaning
 * 
 */
public class Sense implements Parcelable{
	private long id;
	private long wordId;
	private String meaning;
	private String sample;
	private String synonyms; //syn1|syn2|syn3
	public Sense(Parcel in) {
		this.id = in.readLong();
		this.wordId = in.readLong();
		this.meaning = in.readString();
		this.sample = in.readString();
		this.synonyms = in.readString();
	}
	public Sense() { }
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getWordId() {
		return wordId;
	}
	public void setWordId(long wordId) {
		this.wordId = wordId;
	}
	public String getMeaning() {
		return meaning;
	}
	public void setMeaning(String meaning) {
		this.meaning = meaning;
	}
	public String getSample() {
		return sample;
	}
	public void setSample(String sample) {
		this.sample = sample;
	}
	public String getSynonyms() {
		return synonyms;
	}
	public void setSynonyms(String synonyms) {
		this.synonyms = synonyms;
	}
	@Override
	public int describeContents() {
		return 0;
	}
	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeLong(this.id);
		out.writeLong(this.wordId);
		out.writeString(this.meaning);
		out.writeString(this.sample);
		out.writeString(this.synonyms);
	}
	public static final Parcelable.Creator<Sense> CREATOR = new Parcelable.Creator<Sense>() {

		@Override
		public Sense createFromParcel(Parcel source) {
			return new Sense(source);
		}

		@Override
		public Sense[] newArray(int size) {
			return new Sense[size];
		}
		
	};
}

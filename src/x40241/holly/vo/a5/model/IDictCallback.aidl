package x40241.holly.vo.a5.model;
import x40241.holly.vo.a5.model.Term;

oneway interface IDictCallback {
	void lookupCompleted(String entry, in Term term);
	void addCompleted(String entry, long termId);
	void removeCompleted(long termId, long cntRemoved);
}

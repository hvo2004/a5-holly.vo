package x40241.holly.vo.a5.model;
import x40241.holly.vo.a5.model.Term;

oneway interface IQueryLocalCallback {
	void queryLocalCompleted(in List<Term> terms);
}

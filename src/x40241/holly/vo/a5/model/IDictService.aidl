package x40241.holly.vo.a5.model;
import x40241.holly.vo.a5.model.Term;
import x40241.holly.vo.a5.model.IDictCallback;
import x40241.holly.vo.a5.model.IQueryLocalCallback;
import x40241.holly.vo.a5.model.IQueryTermCallback;

interface IDictService {
	boolean lookupEntry(in String entry, in IDictCallback cb);
	boolean removeEntry(in long id, in IDictCallback cb);
	boolean saveEntry(in Term term, in IDictCallback cb);
	boolean queryLocal(IQueryLocalCallback cb);
	boolean queryLocalTerm(in long id, IQueryTermCallback cb);
	long getPid();
}

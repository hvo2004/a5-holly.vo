package x40241.holly.vo.a5.model;
import x40241.holly.vo.a5.model.Term;

oneway interface IQueryTermCallback {
	void queryLocalCompleted(long termId, in Term term);
}

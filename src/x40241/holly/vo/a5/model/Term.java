package x40241.holly.vo.a5.model;

import java.util.ArrayList;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 
 * @author hvo
 * A word entry to dictionary
 *
 */
public class Term implements Parcelable{
	public static final int SRC_LOCAL = 0;
	public static final int SRC_ONLINE = 1;
	
	private long id;
	/** 0: good lookup, -1: not yet or parsing error, others - http response code **/
	private int lookupCode;
	private String entry;
	private int source;
	private List<Word> words = new ArrayList<Word>();
	
	public Term(Parcel in) {
		readFromParcel(in);
	}
	
	public Term(String entry) {
		this();
		this.entry = entry;
	}
	public Term() {
		this.entry = "";
		this.lookupCode = -1; // not yet lookup
		this.id = -1;
		this.source = SRC_LOCAL;
	}

	public void addWord(Word w){
		if (w != null)
			words.add(w);
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getEntry() {
		return entry;
	}

	public void setEntry(String entry) {
		this.entry = entry;
	}

	public int getLookupCode() {
		return lookupCode;
	}

	public void setLookupCode(int lookupCode) {
		this.lookupCode = lookupCode;
	}


	public int getSource() {
		return source;
	}

	public void setSource(int i) {
		this.source = i;
	}

	public List<Word> getWords() {
		return words;
	}
	public void setWords(List<Word> words) {
		this.words = words;
	}
	@Override
	public int describeContents() {
		return 0;
	}
	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeLong(this.id);
		out.writeInt(this.lookupCode);
		out.writeString(this.entry);
		out.writeInt(this.source);
		out.writeTypedList(this.words);
	}
	
	public static final Parcelable.Creator<Term> CREATOR = new Parcelable.Creator<Term>() {

		@Override
		public Term createFromParcel(Parcel in) {
			return new Term(in);
		}

		@Override
		public Term[] newArray(int size) {
			return new Term[size];
		}		
	};

	public void readFromParcel(Parcel in) {
		this.id = in.readLong();
		this.lookupCode = in.readInt();
		this.entry = in.readString();
		this.setSource(in.readByte());
		in.readTypedList(words, Word.CREATOR);
	}
	public int getSenseCount(){
		int counter = 0;
		for (int i = 0, cnt = words.size(); i < cnt; i++)
			counter += words.get(i).getSenses().size();
		return counter;
	}
	public FullSense getSense(int idx){
		FullSense fs = null;
		int index = idx;
		if (index >= 0){
			int sz = 0;
			List<Sense> ls = null;
			for (int i = 0, cnt = words.size(); i < cnt; i++){
				sz = ((ls = words.get(i).getSenses()) == null ? 0 : ls.size());
				if (index < sz){
					Sense s = ls.get(index);
					fs = new FullSense(this.entry, words.get(i).getType(), idx, s);
					return fs;
				}
				else
					index -= sz;
			}
		}
		return fs;
	}
	public List<FullSense> getSenses(){
		List<FullSense> lfs = new ArrayList<FullSense>();
		int idx = 0;
		FullSense fs = null;
		for (int i = 0, cnt = (words == null ? 0 : words.size()); i < cnt; i++){
			List<Sense> ls = words.get(i).getSenses();
			for (int j = 0, jcnt = (ls == null ? 0 : ls.size()); j < jcnt; j++){
				fs = new FullSense(this.entry, words.get(i).getType(), idx++, ls.get(j));
				lfs.add(fs);
			}
		}		
		return lfs;
	}
}

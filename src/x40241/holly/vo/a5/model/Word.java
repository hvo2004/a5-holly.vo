package x40241.holly.vo.a5.model;

import java.util.ArrayList;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;
/**
 * 
 * @author hvo
 * a classified word
 * 
 */
public class Word implements Parcelable{
	private long id;
	private long termId;
	private String type;// adjective, noun
	private List<Sense> senses = new ArrayList<Sense>();
	
	public Word(Parcel in) {
		this.id = in.readLong();
		this.termId = in.readLong();
		this.type = in.readString();
		in.readTypedList(this.senses, Sense.CREATOR);
	}
	public Word() { }
	public void addSense(Sense s){
		if (s != null) this.senses.add(s);
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getTermId() {
		return termId;
	}
	public void setTermId(long termId) {
		this.termId = termId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List<Sense> getSenses() {
		return senses;
	}
	public void setSenses(List<Sense> senses) {
		this.senses = senses;
	}
	@Override
	public int describeContents() {
		return 0;
	}
	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeLong(this.id);
		out.writeLong(this.termId);
		out.writeString(this.type);
		out.writeTypedList(this.senses);
	}
	public static final Parcelable.Creator<Word> CREATOR = new Parcelable.Creator<Word>() {

		@Override
		public Word createFromParcel(Parcel in) {
			return new Word(in);
		}

		@Override
		public Word[] newArray(int size) {
			return new Word[size];
		}		
	};	
}

package x40241.holly.vo.a5.model;


/**
 * 
 * @author hvo
 * Holding full sense for display only
 * 
 */
public class FullSense {
	private int index;
	private String term;
	private String type;
	private String meaning;
	private String sample;
	private String synonyms; //syn1|syn2|syn3
	
	public FullSense(String term, String type, int index, Sense sens) {
		this.index = index;
		this.term = term;
		this.type = type;
		if (sens != null){
			this.meaning = sens.getMeaning();
			this.sample = sens.getSample();
			this.synonyms = sens.getSynonyms();
		}
	}

	public String getTerm() {
		return term;
	}

	public String getType() {
		return String.format("(%s)", type);
	}

	public String getMeaning() {
		return String.format("%d. %s", index + 1, meaning);
	}

	public String getSample() {
		return String.format("...%s", sample);
	}

	public String getSynonyms() {
		return synonyms;
	}
	public String getTypeMeaning(){
		return String.format("%d. (%s) %s", index + 1, type, meaning);
	}
}

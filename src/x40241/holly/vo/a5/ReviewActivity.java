package x40241.holly.vo.a5;

import java.util.List;

import x40241.holly.vo.a5.model.FullSense;
import x40241.holly.vo.a5.model.IDictService;
import x40241.holly.vo.a5.model.IQueryTermCallback;
import x40241.holly.vo.a5.model.IServiceReadyCallback;
import x40241.holly.vo.a5.model.Term;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ViewSwitcher;

public class ReviewActivity extends Activity {
	private static final String TAG = "A5.LocalBookActivity";

	
	IDictService mService;
	
	private ListView lvSenses;
	private CustomListAdapter listAdapter;
	private Term mTerm = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);
        initialize();

        mTerm = getIntent().getParcelableExtra(LocalBookActivity.TERM_DATA);
        listAdapter = new CustomListAdapter(this);
        lvSenses = (ListView)findViewById(R.id.lvSenses);
        lvSenses.setAdapter(listAdapter);
        if (mTerm != null){
        	((TextView)findViewById(R.id.tvTerm)).setText(mTerm.getEntry());
        	retrieveFullSenses();
        }
        final ViewSwitcher viewSwitcher = (ViewSwitcher) findViewById(R.id.viewSwitcher);
		viewSwitcher.setInAnimation(AnimationUtils.loadAnimation(this, android.R.anim.fade_in));
		viewSwitcher.setOutAnimation(AnimationUtils.loadAnimation(this, android.R.anim.fade_out));
		final Switch btnSwitch = (Switch)findViewById(R.id.btnSwitch);
		btnSwitch.setOnCheckedChangeListener(new OnCheckedChangeListener(){

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked)
					viewSwitcher.showNext();
				else
					viewSwitcher.showPrevious();
				
			}
			
		});
    }

    private void initialize() {
        // if not yet ready, mService will be updated in callback
        final DictionaryApp app = DictionaryApp.getSingleton();
        mService = app.getService();
        app.checkServiceReady(new IServiceReadyCallback.Stub() {
    		@Override
    		public void isBounded() throws RemoteException {
    			mService = app.getService();
    		}
    	});
    }
    private class CustomListAdapter extends BaseAdapter {
		private Context context;
		private List<FullSense> list;
		private LayoutInflater layoutInflater;

		CustomListAdapter(Context context) {
			this.context = context;
		}

		public void setList(List<FullSense> list) {
			this.list = list;
		}

		@Override
		public int getCount() {
			return ((list == null) ? 0 : list.size());
		}

		@Override
		public FullSense getItem(int position) {
			return list.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		class ViewHolder {
			int position;
			TextView tvMeaning;
			TextView tvSample;
			TextView tvSyns;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			try {
				if (list == null) {
					return null;
				}
				ViewHolder holder = null;

				if (convertView != null)
					holder = (ViewHolder) convertView.getTag();
				if (holder == null) // not the right view
					convertView = null;
				if (convertView == null) {
					convertView = (LinearLayout) getLayoutInflator().inflate(
							R.layout.list_sens, null);
					holder = new ViewHolder();
					holder.tvMeaning = (TextView) convertView.findViewById(R.id.tvMeaning);
					holder.tvSample = (TextView) convertView.findViewById(R.id.tvSample);
					holder.tvSyns = (TextView) convertView.findViewById(R.id.tvSyns);
					
					convertView.setTag(holder);
				} else
					holder = (ViewHolder) convertView.getTag();
				FullSense fs = list.get(position);
				holder.tvMeaning.setText(fs.getTypeMeaning());
				holder.tvSample.setText(fs.getSample());
				holder.tvSyns.setText(fs.getSynonyms());
			} catch (Exception ex) {
				Log.e(TAG, ex.getMessage());
			}
			return convertView;
		}

		private LayoutInflater getLayoutInflator() {
			if (layoutInflater == null) {
				layoutInflater = (LayoutInflater) this.context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			}
			return layoutInflater;
		}
	}
    private void retrieveFullSenses(){
    	try {
    		mService.queryLocalTerm(mTerm.getId(), mQueryCB);
    	} catch (Exception ex){
    		Log.e(TAG, "retrieveFullSenses: " + ex.getMessage());
    	}    	
    } 
    private void updateSenseList(List<FullSense> lfs){
		listAdapter.setList(lfs);
		listAdapter.notifyDataSetChanged();   	
    }    
    private IQueryTermCallback mQueryCB = new IQueryTermCallback.Stub() {

		@Override
		public void queryLocalCompleted(long termId, Term term)
				throws RemoteException {
			if (mTerm != null && mTerm.getId() == termId && termId > 0 && term != null){
				mTerm = term;
				final List<FullSense> lfs = mTerm.getSenses();
				runOnUiThread(new Runnable(){
					@Override
					public void run() {
						updateSenseList(lfs);
					}
				});	
			}
		}
	};
}

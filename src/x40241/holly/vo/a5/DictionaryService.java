package x40241.holly.vo.a5;

import java.util.List;

import x40241.holly.vo.a5.db.DBVocab;
import x40241.holly.vo.a5.model.IDictCallback;
import x40241.holly.vo.a5.model.IDictService;
import x40241.holly.vo.a5.model.IQueryLocalCallback;
import x40241.holly.vo.a5.model.IQueryTermCallback;
import x40241.holly.vo.a5.model.Term;
import x40241.holly.vo.a5.net.OnlineDictionary;
import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

public class DictionaryService extends Service {
	private static final String TAG = "a5.Service";

	private DBVocab mDbVocab;
	
	@Override
	public void onCreate() {
		super.onCreate();
		Log.d (TAG, "*** onCreate()");
		mDbVocab = new DBVocab(this);
	}
	@Override
	public IBinder onBind(Intent intent) {
		Log.d (TAG, "*** onBind()");
		return new IDictService.Stub() {
			
			@Override
			public boolean saveEntry(Term term, IDictCallback cb) throws RemoteException {
				try {
					new SaveLocalTask(cb).execute(term);
					return true;
				}
				catch (Exception ex){
					Log.e(TAG, ex.getMessage());
				}
				return false;
			}
			
			@Override
			public boolean removeEntry(long id, IDictCallback cb) throws RemoteException {
				try {
					new RemoveLocalTask(cb).execute(id);
					return true;
				}
				catch (Exception ex){
					Log.e(TAG, ex.getMessage());
				}
				return false;
			}
			
			@Override
			public long getPid() throws RemoteException {
				return 5000;
			}


			@Override
			public boolean lookupEntry(String entry, IDictCallback cb)
					throws RemoteException {
				try {
					new LookupTask(cb).execute(entry);
					return true;
				}
				catch (Exception ex){
					Log.e(TAG, ex.getMessage());
				}
				return false;
			}

			@Override
			public boolean queryLocal(IQueryLocalCallback cb)
					throws RemoteException {
				try {
					new QueryTask(cb).execute();
					return true;
				}
				catch (Exception ex){
					Log.e(TAG, ex.getMessage());
				}
				return false;
			}

			@Override
			public boolean queryLocalTerm(long id, IQueryTermCallback cb)
					throws RemoteException {
				try {
					new LookupLocalTask(cb).execute(id);
					return true;
				}
				catch (Exception ex){
					Log.e(TAG, ex.getMessage());
				}
				return false;
			}
		};
	}
	private class RemoveLocalTask extends AsyncTask<Long, Void, Long>{
		IDictCallback mCB;
		long mTermId;
		
		public RemoveLocalTask(IDictCallback cb){
			mCB = cb;
			mTermId = 0;
		}
		@Override
		protected Long doInBackground(Long... params) {
			long cnt = -1;
			try {
				mTermId = (long) params[0];
				cnt = mDbVocab.deleteVocab(mTermId);
			}
			catch (Exception ex){
				Log.e(TAG, "RemoveLocalTask - " + ex.getMessage());
			}
			return cnt;
		}
		@Override
		protected void onPostExecute(Long cntRemoved) {
			try {
				if (mCB != null)
					mCB.removeCompleted(mTermId, cntRemoved);
			}
			catch (Exception ex){
				Log.e(TAG, "RemoveLocalTask callback - " + ex.getMessage());
			}
		}		
	}	
	private class SaveLocalTask extends AsyncTask<Term, Void, Long>{
		IDictCallback mCB;
		String mEntry;
		
		public SaveLocalTask(IDictCallback cb){
			mCB = cb;
			mEntry = "";
		}
		@Override
		protected Long doInBackground(Term... params) {
			long tid = -1;
			try {
				Term term = params[0];
				mEntry = term.getEntry();
				tid = mDbVocab.addVocab(term);
			}
			catch (Exception ex){
				Log.e(TAG, "SaveLocalTask - " + ex.getMessage());
			}
			return tid;
		}
		@Override
		protected void onPostExecute(Long tid) {
			try {
				if (mCB != null)
					mCB.addCompleted(mEntry, tid);
			}
			catch (Exception ex){
				Log.e(TAG, "SaveLocalTask callback - " + ex.getMessage());
			}
		}		
	}
	private class LookupTask extends AsyncTask<String, Void, Term>{
		IDictCallback mCB;
		String mEntry;
		public LookupTask(IDictCallback lookupCB){
			mCB = lookupCB;
			mEntry = "";
		}
		
		@Override
		protected Term doInBackground(String... params) {
			Term term = null;
			try {
				mEntry = params[0];
				term = mDbVocab.getVocab(mEntry);
				if (term == null || term.getLookupCode() != 0)
					term = OnlineDictionary.lookup(mEntry);
			}
			catch (Exception ex){
				Log.e(TAG, ex.getMessage());
			}
			return term;
		}
		
		@Override
		protected void onPostExecute(Term term) {
			try {
				if (mCB != null)
					mCB.lookupCompleted(mEntry, term);
			}
			catch (Exception ex){
				Log.e(TAG, "LookupTask callback - " + ex.getMessage());
			}
		}		
	}
	private class QueryTask extends AsyncTask<Void, Void, List<Term>>{
		IQueryLocalCallback mCB;
		
		public QueryTask(IQueryLocalCallback lookupCB){
			mCB = lookupCB;
		}
		
		@Override
		protected List<Term> doInBackground(Void... params) {
			List<Term> lt = null;
			try {
				lt = mDbVocab.queryVocab();
			}
			catch (Exception ex){
				Log.e(TAG, ex.getMessage());
			}
			return lt;
		}
		
		@Override
		protected void onPostExecute(List<Term> terms) {
			try {
				if (mCB != null)
					mCB.queryLocalCompleted(terms);
			}
			catch (Exception ex){
				Log.e(TAG, "LookupTask callback - " + ex.getMessage());
			}
		}		
	}
	private class LookupLocalTask extends AsyncTask<Long, Void, Term>{
		IQueryTermCallback mCB;
		long termId;
		
		public LookupLocalTask(IQueryTermCallback lookupCB){
			mCB = lookupCB;
			termId = 0;
		}
		
		@Override
		protected Term doInBackground(Long... params) {
			Term term = null;
			try {
				termId = params[0];
				term = mDbVocab.getVocab(termId);
			}
			catch (Exception ex){
				Log.e(TAG, ex.getMessage());
			}
			return term;
		}
		
		@Override
		protected void onPostExecute(Term term) {
			try {
				if (mCB != null)
					mCB.queryLocalCompleted(termId, term);
			}
			catch (Exception ex){
				Log.e(TAG, "LookupLocalTask callback - " + ex.getMessage());
			}
		}		
	}	
}

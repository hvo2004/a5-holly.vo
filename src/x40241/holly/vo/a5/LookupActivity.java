package x40241.holly.vo.a5;

import java.util.List;

import x40241.holly.vo.a5.model.FullSense;
import x40241.holly.vo.a5.model.IDictCallback;
import x40241.holly.vo.a5.model.IDictService;
import x40241.holly.vo.a5.model.IQueryLocalCallback;
import x40241.holly.vo.a5.model.IServiceReadyCallback;
import x40241.holly.vo.a5.model.Term;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher.ViewFactory;


public class LookupActivity extends Activity {
	private static final String TAG = "A5.LookupActivity";
	IDictService mService;

	
	EditText edTerm;
	ProgressDialog mPD;
	LinearLayout llFuncs;
	ImageButton btnPrev;
	ImageButton btnNext;
	TextSwitcher tsWord;
	TextSwitcher tsType;
	TextSwitcher tsSense;
	TextSwitcher tsSample;
	TextSwitcher tsSyns;
	TextView tvExample;
	TextView tvSyn;
	Button btnRemoveLocal;
	Button btnAddLocal;
	
	Term mTerm = null;
	int mTotalSenses = 0;
	int mCurrSenseIdx = -1;
	String mEntry;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lookup);
        initialize();
        initializeTermView();
        
        mPD = null;
        edTerm = (EditText)findViewById(R.id.edTerm);
        llFuncs = (LinearLayout)findViewById(R.id.llFuncs);
        btnPrev = (ImageButton)findViewById(R.id.btnPrev);
        btnNext = (ImageButton)findViewById(R.id.btnNext);
        btnRemoveLocal = (Button)findViewById(R.id.btnRemoveLocal);
		btnAddLocal = (Button)findViewById(R.id.btnAddLocal);
        ((ImageButton)findViewById(R.id.btnLookup)).setOnClickListener(mUserEventListener);
        btnAddLocal.setOnClickListener(mUserEventListener);
        btnRemoveLocal.setOnClickListener(mUserEventListener);
        btnPrev.setOnClickListener(mUserEventListener);
        btnNext.setOnClickListener(mUserEventListener);
        resetTermView();
    }
    private void initializeTermView(){
    	try {
	    	Animation inAnim = AnimationUtils.loadAnimation(this, android.R.anim.slide_in_left);
			Animation outAnim = AnimationUtils.loadAnimation(this, android.R.anim.slide_out_right);		
	    	tsWord = (TextSwitcher)findViewById(R.id.tsWord);
	    	tsType = (TextSwitcher)findViewById(R.id.tsType);
	    	tsSense = (TextSwitcher)findViewById(R.id.tsSense);
	    	tsSample = (TextSwitcher)findViewById(R.id.tsSample);
	    	tsSyns = (TextSwitcher)findViewById(R.id.tsSyns);
	    	tvExample = (TextView)findViewById(R.id.tvExample);
	    	tvSyn = (TextView)findViewById(R.id.tvSyn);
	    	
	    	initializeTextSwitcher(tsWord, new TextViewFact(R.style.LargeWord), inAnim, outAnim);
	    	initializeTextSwitcher(tsType, new TextViewFact(R.style.NormalText), inAnim, outAnim);
	    	initializeTextSwitcher(tsSense, new TextViewFact(R.style.Meaning), inAnim, outAnim);
	    	initializeTextSwitcher(tsSample, new TextViewFact(R.style.NormalText), inAnim, outAnim);
	    	initializeTextSwitcher(tsSyns, new TextViewFact(R.style.SmallHighlightText), inAnim, outAnim);
    	}
    	catch (Exception ex){
    		Log.e(TAG, "initializeTermView: " + ex.getMessage());
    	}
    }
    private void resetTermView(){
    	indicateLocalSource(false);
    	llFuncs.setEnabled(false);
    	mTerm = null;
    	mTotalSenses = 0;
    	mCurrSenseIdx  = -1;
    	displaySense(null);
    }
    private void displaySense(FullSense fs){
    	String sWord = "";
    	String sType = "";
    	String sSense = "";
    	String sSample = "";
    	String sSyns = "";
    	int showLabel = View.INVISIBLE;
    	if (fs != null){
    		showLabel = View.VISIBLE;
    		sWord = fs.getTerm();
    		sType = fs.getType();
    		sSense = fs.getMeaning();
    		sSample = fs.getSample();
    		sSyns = fs.getSynonyms();
    	}
    	tvExample.setVisibility(showLabel);
    	tvSyn.setVisibility(showLabel);
    	updateTextSwitcher(tsWord, sWord);
    	updateTextSwitcher(tsType, sType);
    	updateTextSwitcher(tsSense, sSense);
    	updateTextSwitcher(tsSample, sSample);
    	updateTextSwitcher(tsSyns, sSyns);    	
    }
    private void switchTermButtons(){
    	indicateLocalSource(mTerm != null && mTerm.getSource() == Term.SRC_LOCAL);
    }
    private void displayTermView(){
    	mTotalSenses = (mTerm == null ? 0 : mTerm.getSenseCount());
    	if (mTotalSenses <= 0){
    		resetTermView();
    		updateTextSwitcher(tsType, getString(R.string.msg_def_not_found));
    	}
    	else{
    		llFuncs.setEnabled(true);
    		mCurrSenseIdx = 0;
    		indicateLocalSource(mTerm.getSource() == Term.SRC_LOCAL);
    		displaySense();
    	}
    }
    private void indicateLocalSource(boolean local){
    	if (local){
    		btnRemoveLocal.setVisibility(View.VISIBLE);
    		btnAddLocal.setVisibility(View.GONE);
    	} else {
    		btnRemoveLocal.setVisibility(View.GONE);
    		btnAddLocal.setVisibility(View.VISIBLE);   		
    	}
    }
    private boolean hasNext(){
    	return (mCurrSenseIdx < this.mTotalSenses - 1);
    }
    private boolean hasPrev(){
    	return (this.mCurrSenseIdx > 0);
    }
    private void displaySense(){
    	btnNext.setEnabled(hasNext());
    	btnPrev.setEnabled(hasPrev());
    	try {
    		FullSense sens = mTerm.getSense(mCurrSenseIdx);
    		if (sens == null){
    			resetTermView();
    			return;
    		}
    		displaySense(sens);
    	}
    	catch (Exception ex){
    		Log.e(TAG, "displaySense: " + ex.getMessage());
    	}
    }

	private void initializeTextSwitcher(TextSwitcher ts, ViewFactory vf, Animation inAnim, Animation outAnim){
		ts.setFactory(vf);
		ts.setInAnimation(inAnim);
		ts.setOutAnimation(outAnim);
	}
	private void updateTextSwitcher(TextSwitcher ts, String s){
		TextView tv = null;
		if ((tv = (TextView)ts.getCurrentView()) == null)
			ts.setCurrentText(s);
		else if (tv.getText().toString().compareTo(s) != 0)
			ts.setText(s);	
	} 	
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_localbook) {
            Intent intent = new Intent(this, LocalBookActivity.class);
            this.startActivity(intent);        	
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    private void initialize() {
        // if not yet ready, mService will be updated in callback
        final DictionaryApp app = DictionaryApp.getSingleton();
        mService = app.getService();
        app.checkServiceReady(new IServiceReadyCallback.Stub() {
    		@Override
    		public void isBounded() throws RemoteException {
    			mService = app.getService();
    		}
    	});
    }

    private IDictCallback mServiceCB = new IDictCallback.Stub() {
		
		@Override
		public void lookupCompleted(String entry, Term term) throws RemoteException {
			try {
				if (entry != null && entry.compareToIgnoreCase(mEntry) == 0){
					mTerm = term;
				}
				runOnUiThread(new Runnable(){
					@Override
					public void run() {
						displayTermView();
					}
				});					
			}
			catch (Exception ex){
				Log.e(TAG, ex.getMessage());
			}
			finally{
				showProgress(false, 0);
			}
		}

		@Override
		public void addCompleted(String entry, long termId)
				throws RemoteException {
			try {
				if (entry != null && entry.compareToIgnoreCase(mEntry) == 0){
					final boolean added = (termId > 0);
					if (added){
						mTerm.setId(termId);
						mTerm.setSource(Term.SRC_LOCAL);
					}
					runOnUiThread(new Runnable(){
						@Override
						public void run() {
							showToastMessage(mTerm.getEntry(), added ? R.string.msg_save_succ : R.string.msg_save_fail);
							switchTermButtons();
						}
					});					
				}
			}
			catch (Exception ex){
				Log.e(TAG, ex.getMessage());
			}
			finally{
				showProgress(false, 0);
			}
		}

		@Override
		public void removeCompleted(long termId, long cntRemoved)
				throws RemoteException {
			try {
				if (mTerm != null && termId > 0 && mTerm.getId() == termId){
					final boolean removed = (cntRemoved >= 0);
					runOnUiThread(new Runnable(){
						@Override
						public void run() {
							showToastMessage(mTerm.getEntry(), removed ? R.string.msg_delete_succ : R.string.msg_delete_fail);
							if (removed)
								mTerm.setSource(Term.SRC_ONLINE);
							resetTermView();
						}
					});
				}
			}
			catch (Exception ex){
				Log.e(TAG, ex.getMessage());
			}
			finally{
				showProgress(false, 0);
			}
		}
	};
	private void showToastMessage(String entry, int msgId){
		try {
			Toast.makeText(this, getString(msgId, entry), Toast.LENGTH_LONG).show();
		}
		catch (Exception ex){
			Log.e(TAG, "showToastMessage:" + ex.getMessage());
		}
	}
	private void showInfoMessage(String title, String msg){
		AlertDialog.Builder db = new AlertDialog.Builder(this);
		db.setTitle(title);
		db.setMessage(msg);
		db.setPositiveButton(R.string.btn_ok, null);
		db.show();
	}
	private void showProgress(boolean show, int msgId){
		if (show){
			if (mPD == null){
				ProgressDialog pd = new ProgressDialog(this);
				pd.setIndeterminate(true);
				pd.setCancelable(false);
				if (msgId > 0)
					pd.setMessage(getString(msgId));
				mPD = pd;
			}
			mPD.show();
		}
		else if (mPD != null)
		{
			mPD.dismiss();
			mPD = null;
		}
	}
	private void lookUp(){
		this.resetTermView();
		mEntry = edTerm.getText().toString().trim().toLowerCase();
		if (mEntry.length() == 0){
			showInfoMessage(getString(R.string.title_lookup), getString(R.string.msg_lookup_invalid));
			return;
		}			
		try {
			showProgress(true, R.string.msg_lookup);
			mService.lookupEntry(mEntry, mServiceCB);
		}
		catch (Exception ex){
			showProgress(false, 0);
		}		
	}
	private void addToLocal(){
		try {
			if (mTerm != null){
				showProgress(true, R.string.msg_savelocal);
				mService.saveEntry(mTerm, mServiceCB);
			}
		}
		catch (Exception ex){
			showProgress(false, 0);
		}			
	}
	private void removeFromLocal(){
		try {
			if (mTerm != null){
				showProgress(true, R.string.msg_savelocal);
				mService.removeEntry(mTerm.getId(), mServiceCB);
			}
		}
		catch (Exception ex){
			showProgress(false, 0);
		}			
	}	
	private void viewNext(){
		mCurrSenseIdx++;
		this.displaySense();
	}
	private void viewPrevious(){
		mCurrSenseIdx--;
		this.displaySense();
	}
	private OnClickListener mUserEventListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			int btnId = v.getId();
			if (btnId == R.id.btnLookup)
				lookUp();
			else if (btnId == R.id.btnAddLocal)
				addToLocal();
			else if (btnId == R.id.btnNext)
				viewNext();
			else if (btnId == R.id.btnPrev)
				viewPrevious();
			else if (btnId == R.id.btnRemoveLocal)
				removeFromLocal();
		}
	};
	private class TextViewFact implements ViewFactory{
		private int mStyleId;
		public TextViewFact(int styleId) {
			mStyleId = styleId;
		}

		@Override
		public View makeView() {
			TextView tv = new TextView(LookupActivity.this);
			tv.setTextAppearance(LookupActivity.this, mStyleId);
			return tv;
		}
	};	
}

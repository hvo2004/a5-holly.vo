package x40241.holly.vo.a5;

import x40241.holly.vo.a5.model.IDictService;
import x40241.holly.vo.a5.model.IServiceReadyCallback;
import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.util.Log;

public class DictionaryApp extends Application {
	private static final String TAG = "A5.DictionaryApp";
	private static final String ACTION_DICT_SERVICE = "x40241.holly.vo.a5.service.DictService";	
	
	private static DictionaryApp _singleton = null;
	private IDictService mService;
	
	boolean mBound = false;// bounded to stock service?
	Intent mIntentDService;
	final private RemoteCallbackList<IServiceReadyCallback> _servicesReadyCBs = new RemoteCallbackList<IServiceReadyCallback>();
	
	public IDictService getService() {
		return mService;
	}
	
	public static DictionaryApp getSingleton() {
		return _singleton;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		Log.d (TAG, "onCreate");
		_singleton = this;
		initialize();
	}

	@Override
	public void onTerminate() {
		super.onTerminate();
		unbindServices();
        Log.d (TAG, "onTerminate");
	}
	void unbindServices(){
        if (mBound)
        {
            unbindService(mConnection);
            mBound = false;
        }		
	}

    private void initialize() {
        Log.d (TAG, "*** initialize(): STARTING");
        mIntentDService = new Intent(ACTION_DICT_SERVICE);
        startService(mIntentDService);
        bindService(mIntentDService, mConnection, Context.BIND_AUTO_CREATE);
        mBound = true;
        Log.d (TAG, "*** initialize(): COMPLETED");
    }
    ServiceConnection mConnection = new ServiceConnection(){

		@Override
		public void onServiceConnected(ComponentName className, IBinder binder) {
			try {
				mService = IDictService.Stub.asInterface(binder);
				broadcastServiceReady();
			}
			catch (Exception ex){
				Log.e(TAG, "onServiceConnected: " + ex.getMessage());
			}
		}

		@Override
		public void onServiceDisconnected(ComponentName className) {
			mService = null;
		}
    	
    };
    
    public void checkServiceReady(IServiceReadyCallback cb){
    	_servicesReadyCBs.register(cb);
    }
    public void broadcastServiceReady(){
    	try {
    		int count = this._servicesReadyCBs.beginBroadcast();
    		for (int i = 0; i < count; i++){
    			try {
    				this._servicesReadyCBs.getBroadcastItem(i).isBounded();
    			} catch (RemoteException e){
    				e.printStackTrace();
    			}
    		}
    		this._servicesReadyCBs.finishBroadcast();
    	}
    	catch (Exception ex){
    		Log.e(TAG, "broadcastServiceReady: " + ex.getMessage());
    	}
    }
}

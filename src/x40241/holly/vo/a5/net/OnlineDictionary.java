package x40241.holly.vo.a5.net;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import x40241.holly.vo.a5.model.Term;
import android.util.Log;

public class OnlineDictionary{
	private static String TAG = "A5.net";
	private static String API_URL_FMT = "http://www.dictionaryapi.com/api/v1/references/thesaurus/xml/%s?key=%s";
	private static String API_KEY = "ae883387-9486-4aaa-bafc-34a4f211f76d";
	
	public static Term lookup(String term) {
		Term tm = null;
		URL url = null;
        InputStream in = null;
		try {
			Log.i(TAG, "look up: " + term);
			tm = new Term(term);
			String surl = String.format(API_URL_FMT, term, API_KEY);
			Log.i(TAG, "look up url: " + surl);
            url = new URL(surl);
            
            URLConnection connection;
            connection = url.openConnection();

            HttpURLConnection httpConnection = (HttpURLConnection) connection;
            int responseCode = httpConnection.getResponseCode();
            
            if (responseCode != HttpURLConnection.HTTP_OK) {
            	Log.d(TAG, String.format("OnlineLookUpTask returns error Response Code: %d", responseCode));
            	tm.setLookupCode(responseCode);
            	return tm;
            }
            in = httpConnection.getInputStream();
            new TermSAX(tm).parse(in);
            tm.setSource(Term.SRC_ONLINE);
		}
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (Throwable t) {
            //  At least ensure the thread always ends orderly, even 
            //  in the event of something completely unexpected
            t.printStackTrace();
        }
        finally {
            if (in != null) {
                try {
                    in.close();
                    in = null;
                }
                catch (IOException e) { /* ignore */ }
            }
        }
		return tm;
	}

}

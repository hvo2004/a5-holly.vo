package x40241.holly.vo.a5.net;

import java.io.InputStream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;
import x40241.holly.vo.a5.model.Sense;
import x40241.holly.vo.a5.model.Term;
import x40241.holly.vo.a5.model.Word;
/**
 * 
 * @author hvo
 *
 * Sample lookup
 * 
 * <entry_list version="1.0">
	<entry id="gratuitous">
		<term>
			<hw>gratuitous</hw>
		</term>
		<fl>adjective</fl>
		<sens>
			<sn>1</sn>
			<mc>not costing or charging anything</mc>
			<vi>they will throw in a <it>gratuitous</it> box of chocolates when you spend $30 or more in their shop</vi>
			<syn>complimentary, costless, gratis, gratuitous</syn>
			<rel>nominal; bestowed, donated, given; pro bono; discretionary, freewill, optional, voluntary; honorary, uncompensated, unpaid</rel>
			<near>paid; costly, dear, expensive, high</near>
		</sens>
		<sens>
			<sn>2</sn>
			<mc>not needed by the circumstances or to accomplish an end</mc>
			<vi>that violent scene was completely <it>gratuitous</it> and didn't need to be in the movie at all</vi>
			<syn>dispensable, gratuitous, inessential, needless, nonessential, uncalled-for, unessential, unwarranted</syn>
			<rel>discretionary, elective, optional; extra, extraneous, irrelative, irrelevant, redundant, superfluous</rel>
			<near>all-important, crucial, important, vital; imperative, pressing, urgent</near>
			<ant>essential, indispensable, necessary, needed, needful, required</ant>
		</sens>
	</entry>
</entry_list>
 * 
 */
public final class TermSAX extends DefaultHandler {
	private static final String TAG = "A5.TermSAX";
	private static final boolean DEBUG = true;
    private static final String TERM_ELEM   = "entry_list"; // doc root
    private static final String WORD_ELEM  = "entry";
    private static final String SENSE_ELEM = "sens";
    private static final String TYPE_ELEM = "fl";
    private static final String MEANING_ELEM = "mc";
    private static final String SAMPLE_ELEM = "vi";
    private static final String SYN_ELEM = "syn";
    
	
	private Term mTerm;
	private Word mWord;
	private Sense mSense;
	private StringBuilder buffer = new StringBuilder();
	
	public TermSAX(Term t){
		mTerm = t;
	}
	public Term parse(InputStream is) throws Exception{
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser saxParser = factory.newSAXParser();
        saxParser.parse(is, this);
		return mTerm;
	}
    @Override
    public void characters (char[] ch, int start, int length)
        throws SAXException
    {
        super.characters(ch, start, length);
        buffer.append(ch, start, length);
    }
    public void startElement (String uri, String localName, String name, Attributes attributes)
            throws SAXException
    {
        if (DEBUG) Log.d (TAG, "startElement="+localName);
        super.startElement (uri, localName, name, attributes);
        if (localName.equals(WORD_ELEM)) {
        	mWord = new Word();
        	return;
        }
        if (localName.equals(SENSE_ELEM)) {
        	mSense = new Sense();
        	return;
        }
      
    }
    public void endElement (String uri, String localName, String name)
            throws SAXException
    {
        if (DEBUG) Log.d (TAG, "endElement="+localName);
        super.endElement(uri, localName, name);
        if (localName.equals(WORD_ELEM)) {
        	mTerm.addWord(mWord);
        	mWord = null;
        	return;
        }
        if (localName.equals(SENSE_ELEM)) {
        	mWord.addSense(mSense);
        	mSense = null;
        	return;
        }
        if (localName.equals(TERM_ELEM)) {
        	mTerm.setLookupCode(0); // all ok
        	return;
        }        
        parseEndElement (uri, localName, name, buffer.toString());
        buffer.delete(0, buffer.length());
    }
    private void parseEndElement (String uri, String localName, String name, String value)
    {
        value = value.trim();  //  may not be appropriate for all parsing situations
        if (DEBUG) {
            Log.d (TAG, "localName="+localName);
            Log.d (TAG, "value="+value);
        }
        if (localName.equals(TYPE_ELEM)) {
        	mWord.setType(value);
        	return;
        }
        if (localName.equals(MEANING_ELEM)) {
        	mSense.setMeaning(value);
            return;
        }
        if (localName.equals(SAMPLE_ELEM)) {
        	mSense.setSample(value.replaceAll("<it>", "").replaceAll("</it>", ""));
            return;
        }
        if (localName.equals(SYN_ELEM)) {
        	mSense.setSynonyms(value);
            return;
        }
    }    
}

package x40241.holly.vo.a5;

import java.util.List;

import x40241.holly.vo.a5.model.IDictService;
import x40241.holly.vo.a5.model.IQueryLocalCallback;
import x40241.holly.vo.a5.model.IServiceReadyCallback;
import x40241.holly.vo.a5.model.Term;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher.ViewFactory;

public class LocalBookActivity extends Activity {
	private static final String TAG = "A5.LocalBookActivity";
	public static final String TERM_DATA = "A5.term";
	
	IDictService mService;
	
	private ListView lvTerms;
	private CustomListAdapter listAdapter;
	private TextView tvEmpty;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_localbook);
        initialize();
        
        listAdapter = new CustomListAdapter(this);
        lvTerms = (ListView)findViewById(R.id.lvTerms);
        lvTerms.setAdapter(listAdapter);
        lvTerms.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View view,
					int position, long id) {
				Term term = listAdapter.getItem(position);
				Intent intent = new Intent(LocalBookActivity.this, ReviewActivity.class);
				intent.putExtra(TERM_DATA, term);
				LocalBookActivity.this.startActivity(intent);
			}
		});
		tvEmpty = (TextView)findViewById(R.id.tvEmptyList);
		tvEmpty.setVisibility(View.INVISIBLE);
        retrieveLocalBook();
    }
 
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_lookup) {
            Intent intent = new Intent(this, LookupActivity.class);
            this.startActivity(intent);
        	return true;
        }
        return super.onOptionsItemSelected(item);
    }
    private void initialize() {
        // if not yet ready, mService will be updated in callback
        final DictionaryApp app = DictionaryApp.getSingleton();
        mService = app.getService();
        app.checkServiceReady(new IServiceReadyCallback.Stub() {
    		@Override
    		public void isBounded() throws RemoteException {
    			mService = app.getService();
    		}
    	});
    }
    class CustomListAdapter extends BaseAdapter {
		private Context context;
		private List<Term> list;
		private LayoutInflater layoutInflater;

		CustomListAdapter(Context context) {
			this.context = context;
		}

		public void setList(List<Term> list) {
			this.list = list;
		}

		@Override
		public int getCount() {
			return ((list == null) ? 0 : list.size());
		}

		@Override
		public Term getItem(int position) {
			return list.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		class ViewHolder {
			int position;
			TextSwitcher textView;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			try {
				if (list == null) {
					return null;
				}
				ViewHolder holder = null;

				if (convertView != null)
					holder = (ViewHolder) convertView.getTag();
				if (holder == null) // not the right view
					convertView = null;
				if (convertView == null) {
					convertView = (LinearLayout) getLayoutInflator().inflate(
							R.layout.list_item, null);
					holder = new ViewHolder();
					holder.textView = (TextSwitcher) convertView
							.findViewById(R.id.text_view);
					Animation inAnim = AnimationUtils.loadAnimation(context, android.R.anim.fade_in);
					Animation outAnim = AnimationUtils.loadAnimation(context, android.R.anim.fade_out);
					holder.textView.setInAnimation(inAnim);
					holder.textView.setOutAnimation(outAnim);
					holder.textView.setFactory(new ViewFactory(){

						@Override
						public View makeView() {
							TextView text = new TextView(context);
							text.setTextAppearance(LocalBookActivity.this, R.style.MediumHighlightText);
							return text;
						}
						
					});					
					convertView.setTag(holder);
				} else
					holder = (ViewHolder) convertView.getTag();

				Term term = list.get(position);
				TextView tv = null;
				String entry = term.getEntry();
				if ((tv = (TextView)holder.textView.getCurrentView()) == null)
					holder.textView.setCurrentText(entry);
				else if (tv.getText().toString().compareTo(entry) != 0)
					holder.textView.setText(entry);				
			} catch (Exception ex) {
				Log.e(TAG, ex.getMessage());
			}
			return convertView;
		}

		private LayoutInflater getLayoutInflator() {
			if (layoutInflater == null) {
				layoutInflater = (LayoutInflater) this.context
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			}
			return layoutInflater;
		}
	}
    private void updateTermList(List<Term> terms){
		listAdapter.setList(terms);
		listAdapter.notifyDataSetChanged();
		tvEmpty.setVisibility(View.VISIBLE);
		lvTerms.setEmptyView(tvEmpty);
    }
    private void retrieveLocalBook(){
    	try {
    		mService.queryLocal(this.mQueryCB);    	
    	} catch (Exception ex){
    		Log.e(TAG, "retrieveLocalBook: " + ex.getMessage());
    	}
    }
    private IQueryLocalCallback mQueryCB = new IQueryLocalCallback.Stub() {
		
		@Override
		public void queryLocalCompleted(List<Term> terms) throws RemoteException {
			final List<Term> lt = terms;
			runOnUiThread(new Runnable(){
				@Override
				public void run() {
					updateTermList(lt);
				}
			});			
			
		}
	};
}

package x40241.holly.vo.a5.db;

import java.util.ArrayList;
import java.util.List;

import x40241.holly.vo.a5.model.Sense;
import x40241.holly.vo.a5.model.Term;
import x40241.holly.vo.a5.model.Word;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

public class DBVocab {
	private static final String TAG = "VOCAB_DB";
	private static final int DB_VERSION = 1;
	private static final String DB_NAME = "db.vocab";
	
	
	private Context mContext;
	private SQLiteDatabase mDb;
	private SQLiteStatement mStmtInsertTerm;
	private SQLiteStatement mStmtInsertWord;
	private SQLiteStatement mStmtInsertSense;
	
	public DBVocab(Context context){
		mContext = context;
		mDb = new VocabDBHelper(mContext).getWritableDatabase();
		mStmtInsertTerm = mDb.compileStatement(TABLE_TERM.STMT_INSERT);
		mStmtInsertWord = mDb.compileStatement(TABLE_WORD.STMT_INSERT);
		mStmtInsertSense = mDb.compileStatement(TABLE_SENSE.STMT_INSERT);
	}
	public long addVocab(Term term){
		long val = -1;
		try {
			val = insertTerm(term);
		}
		catch (Exception ex){
			Log.e(TAG, ex.getMessage());
		}
		return val;
	}
	public Term getVocab(String entry){
		Term t = null;
		try {
			String selection = String.format("%s='%s'", TABLE_TERM.TERM.columnName, entry);
			t = queryVocab(selection);
		}
		catch (Exception ex){
			Log.e(TAG, "getVocab: " + ex.getMessage());
		}		
		return t;
	}
	public Term getVocab(long termId){
		Term t = null;
		try {
			String selection = String.format("%s=%d", TABLE_TERM.ID.columnName, termId);
			t = queryVocab(selection);
			if (t != null){
				List<Word> lw = getWords(t.getId());
				Word w = null;
				for (int i = 0, cnt = (lw == null ? 0 : lw.size()); i < cnt; i++){
					if ((w = lw.get(i)) != null)
						w.setSenses(getSenses(w.getId()));
				}
				t.setWords(lw);
			}
		}
		catch (Exception ex){
			Log.e(TAG, "getVocab: " + ex.getMessage());
		}		
		return t;
	}	
		
	public List<Term> queryVocab(){
		List<Term> lt = new ArrayList<Term>();
		try {
			Term t = null;
			String selection = null;
			String orderby = TABLE_TERM.TERM.columnName;
			Cursor cursor = mDb.query(TABLE_TERM.TableName, TABLE_TERM.COL_ALL, selection, null, null, null, orderby);
			if (cursor.moveToFirst()){
				do {
					t = new Term();
					t.setId(cursor.getLong(TABLE_TERM.ID.columnIndex));
					t.setEntry(cursor.getString(TABLE_TERM.TERM.columnIndex));
					t.setWords(null);
					lt.add(t);
				} while (cursor.moveToNext());
			}
			if (cursor != null && !cursor.isClosed())
				cursor.close();
		}
		catch (Exception ex){
			Log.e(TAG, "getVocab: " + ex.getMessage());
		}		
		return lt;
	}		
	public long deleteVocab(long id){
		long cnt = -1;
		try {
			String cond = String.format("%s = %d", TABLE_TERM.ID.columnName, id);
			cnt = mDb.delete(TABLE_TERM.TableName, cond, null);					
		}
		catch (Exception ex){
			Log.e(TAG, ex.getMessage());
		}
		return cnt;		
	}
	
	// ================================
	// private section
	// ================================
	private Term queryVocab(String selection){
		Term t = null;
		try {
			Cursor cursor = mDb.query(TABLE_TERM.TableName, TABLE_TERM.COL_ALL, selection, null, null, null, null, "1");
			if (cursor.moveToFirst()){
				t = new Term();
				t.setId(cursor.getLong(TABLE_TERM.ID.columnIndex));
				t.setEntry(cursor.getString(TABLE_TERM.TERM.columnIndex));
			}
			if (cursor != null && !cursor.isClosed())
				cursor.close();
			if (t != null){
				List<Word> lw = getWords(t.getId());
				Word w = null;
				for (int iw = 0, cntw = lw.size(); iw < cntw; iw++){
					if ((w = lw.get(iw)) != null)
						w.setSenses(getSenses(w.getId()));
				}
				t.setWords(lw);
				t.setLookupCode(0);
				t.setSource(Term.SRC_LOCAL);
			}
		}
		catch (Exception ex){
			Log.e(TAG, "getVocab: " + ex.getMessage());
		}		
		return t;
	}	
	private List<Word> getWords(long tid){
		List<Word> lw = new ArrayList<Word>();
		try {
			String selection = String.format("%s=%d", TABLE_WORD.TERM_ID.columnName, tid);
			Cursor cursor = mDb.query(TABLE_WORD.TableName, TABLE_WORD.COL_ALL, selection, null, null, null, null);
			if (cursor.moveToFirst()){
				Word w = null;
				do {
					w = new Word();
					w.setId(cursor.getLong(TABLE_WORD.ID.columnIndex));
					w.setTermId(cursor.getLong(TABLE_WORD.TERM_ID.columnIndex));
					w.setType(cursor.getString(TABLE_WORD.TYPE.columnIndex));
					lw.add(w);
				} while (cursor.moveToNext());
			}
			if (cursor != null && !cursor.isClosed())
				cursor.close();
		}
		catch (Exception ex){
			Log.e(TAG, "getWords: " + ex.getMessage());
		}		
		return lw;
	}
	private List<Sense> getSenses(long wid){
		List<Sense> ls = new ArrayList<Sense>();
		try {
			String selection = String.format("%s=%d", TABLE_SENSE.WORD_ID.columnName, wid);
			Cursor cursor = mDb.query(TABLE_SENSE.TableName, TABLE_SENSE.COL_ALL, selection, null, null, null, null);
			if (cursor.moveToFirst()){
				Sense s = null;
				do {
					s = new Sense();
					s.setId(cursor.getLong(TABLE_SENSE.ID.columnIndex));
					s.setWordId(cursor.getLong(TABLE_SENSE.WORD_ID.columnIndex));
					s.setMeaning(cursor.getString(TABLE_SENSE.MEANING.columnIndex));
					s.setSample(cursor.getString(TABLE_SENSE.SAMPLE.columnIndex));
					s.setSynonyms(cursor.getString(TABLE_SENSE.SYNONYMS.columnIndex));
					ls.add(s);
				} while (cursor.moveToNext());
			}
			if (cursor != null && !cursor.isClosed())
				cursor.close();
		}
		catch (Exception ex){
			Log.e(TAG, "getSenses: " + ex.getMessage());
		}		
		return ls;
	}	
	private long insertTerm(Term term){
		long val = -1;
		try {
			if (term != null){
				mStmtInsertTerm.bindString(TABLE_TERM.TERM.columnIndex, term.getEntry());
				val = mStmtInsertTerm.executeInsert();
				if (val > 0){
					List<Word> lw = term.getWords();
					for (int i = 0, cnt = lw.size(); i < cnt; i++)
						insertWord(lw.get(i), val);
					term.setId(val);
				}
			}
		}
		catch (Exception ex){
			Log.e(TAG, "insertTerm: " + ex.getMessage());
		}
		return val;		
	}
	private long insertWord(Word word, long tid){
		long val = -1;
		try {
			if (word != null && tid > 0){
				mStmtInsertWord.bindLong(TABLE_WORD.TERM_ID.columnIndex, tid);
				mStmtInsertWord.bindString(TABLE_WORD.TYPE.columnIndex, word.getType());
				val = mStmtInsertWord.executeInsert();
				if (val > 0){
					List<Sense> ls = word.getSenses();
					for (int i = 0, cnt = word.getSenses().size(); i < cnt; i++)
						insertSense(ls.get(i), val);
					word.setId(val);
				}
			}
		}
		catch (Exception ex){
			Log.e(TAG, "insertWord: " + ex.getMessage());
		}
		return val;			
	}
	private long insertSense(Sense sense, long wid){
		long val = -1;
		try {
			if (sense != null && wid > 0){
				mStmtInsertSense.bindLong(TABLE_SENSE.WORD_ID.columnIndex, wid);
				mStmtInsertSense.bindString(TABLE_SENSE.MEANING.columnIndex, sense.getMeaning());
				mStmtInsertSense.bindString(TABLE_SENSE.SAMPLE.columnIndex, sense.getSample());
				mStmtInsertSense.bindString(TABLE_SENSE.SYNONYMS.columnIndex, sense.getSynonyms());
				if ((val = mStmtInsertSense.executeInsert()) > 0)
					sense.setId(val);
			}
		}
		catch (Exception ex){
			Log.e(TAG, "insertSense: " + ex.getMessage());
		}
		return val;		
	}
	private static class VocabDBHelper extends SQLiteOpenHelper {
		public VocabDBHelper(Context context) {
			super(context, DB_NAME, null, DB_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			Log.d(TAG, "onCreate");
			db.execSQL(TABLE_TERM.STMT_CREATE);
			db.execSQL(TABLE_WORD.STMT_CREATE);
			db.execSQL(TABLE_SENSE.STMT_CREATE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			Log.d(TAG, "onUpgrade");
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_SENSE.TableName);
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_WORD.TableName);
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_TERM.TableName);
			onCreate(db);
		}
	}
	enum TABLE_TERM {
		ID("_id"), TERM("term");

		public static final String TableName = "Term_t";
		public static final String[] COL_ALL = new String[]{
			ID.columnName, TERM.columnName
		};
		public final int columnIndex = this.ordinal();
		public final String columnName;

		private TABLE_TERM(final String columnName) {
			this.columnName = columnName;
		}
		public static final String STMT_CREATE = "CREATE TABLE "
				+ TableName + " ("
				+ ID.columnName + " INTEGER PRIMARY KEY AUTOINCREMENT, "
				+ TERM.columnName + " TEXT NOT NULL UNIQUE);";
		public static final String STMT_INSERT = 
			String.format("insert into %s (%s) values (?);", TableName, TERM.columnName);
	}
	enum TABLE_WORD {
		ID("_id"), TERM_ID("termId"), TYPE("type"); 

		public static final String TableName = "Word_t";
		public static final String[] COL_ALL = new String[]{
			ID.columnName, TERM_ID.columnName, TYPE.columnName
		};
		public final int columnIndex = this.ordinal();
		public final String columnName;

		private TABLE_WORD(final String columnName) {
			this.columnName = columnName;
		}
		public static final String STMT_CREATE = "CREATE TABLE "
				+ TableName + " ("
				+ ID.columnName + " INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ TERM_ID.columnName + " INTEGER NOT NULL,"
				+ TYPE.columnName + " TEXT NOT NULL,"
				+ "FOREIGN KEY(" + TERM_ID.columnName
				+ ") REFERENCES " + TABLE_TERM.TableName + "("
				+ TABLE_TERM.ID.columnName + ") ON DELETE CASCADE);";
		public static final String STMT_INSERT = 
				String.format("insert into %s (%s, %s) values (?, ?);", TableName, TERM_ID.columnName, TYPE.columnName);
	}
	enum TABLE_SENSE {
		ID("_id"), WORD_ID("wordId"), MEANING("meaning"), SAMPLE("sample"), SYNONYMS("synonyms");  

		public static final String TableName = "Sense_t";
		public static final String[] COL_ALL = new String[]{
			ID.columnName, WORD_ID.columnName, MEANING.columnName, SAMPLE.columnName, SYNONYMS.columnName
		};
		public final int columnIndex = this.ordinal();
		public final String columnName;

		private TABLE_SENSE(final String columnName) {
			this.columnName = columnName;
		}
		public static final String STMT_CREATE = "CREATE TABLE "
				+ TableName + " ("
				+ ID.columnName + " INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ WORD_ID.columnName + " INTEGER NOT NULL,"
				+ MEANING.columnName + " TEXT NOT NULL,"
				+ SAMPLE.columnName + " TEXT,"
				+ SYNONYMS.columnName + " TEXT,"
				+ "FOREIGN KEY(" + WORD_ID.columnName
				+ ") REFERENCES " + TABLE_WORD.TableName + "("
				+ TABLE_WORD.ID.columnName + ") ON DELETE CASCADE);";
		public static final String STMT_INSERT =
				String.format("insert into %s (%s, %s, %s, %s) values (?, ?, ?, ?);", TableName, 
						WORD_ID.columnName, MEANING.columnName, SAMPLE.columnName, SYNONYMS.columnName);
	}	
}

/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: C:\\Users\\hvo\\Dropbox\\EDU\\COMSCI\\Android\\Android_II\\Assignment\\A5-Holly.Vo\\src\\x40241\\holly\\vo\\a5\\model\\IDictCallback.aidl
 */
package x40241.holly.vo.a5.model;
public interface IDictCallback extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements x40241.holly.vo.a5.model.IDictCallback
{
private static final java.lang.String DESCRIPTOR = "x40241.holly.vo.a5.model.IDictCallback";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an x40241.holly.vo.a5.model.IDictCallback interface,
 * generating a proxy if needed.
 */
public static x40241.holly.vo.a5.model.IDictCallback asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof x40241.holly.vo.a5.model.IDictCallback))) {
return ((x40241.holly.vo.a5.model.IDictCallback)iin);
}
return new x40241.holly.vo.a5.model.IDictCallback.Stub.Proxy(obj);
}
@Override public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_lookupCompleted:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
x40241.holly.vo.a5.model.Term _arg1;
if ((0!=data.readInt())) {
_arg1 = x40241.holly.vo.a5.model.Term.CREATOR.createFromParcel(data);
}
else {
_arg1 = null;
}
this.lookupCompleted(_arg0, _arg1);
return true;
}
case TRANSACTION_addCompleted:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
long _arg1;
_arg1 = data.readLong();
this.addCompleted(_arg0, _arg1);
return true;
}
case TRANSACTION_removeCompleted:
{
data.enforceInterface(DESCRIPTOR);
long _arg0;
_arg0 = data.readLong();
long _arg1;
_arg1 = data.readLong();
this.removeCompleted(_arg0, _arg1);
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements x40241.holly.vo.a5.model.IDictCallback
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
@Override public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
@Override public void lookupCompleted(java.lang.String entry, x40241.holly.vo.a5.model.Term term) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(entry);
if ((term!=null)) {
_data.writeInt(1);
term.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
mRemote.transact(Stub.TRANSACTION_lookupCompleted, _data, null, android.os.IBinder.FLAG_ONEWAY);
}
finally {
_data.recycle();
}
}
@Override public void addCompleted(java.lang.String entry, long termId) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(entry);
_data.writeLong(termId);
mRemote.transact(Stub.TRANSACTION_addCompleted, _data, null, android.os.IBinder.FLAG_ONEWAY);
}
finally {
_data.recycle();
}
}
@Override public void removeCompleted(long termId, long cntRemoved) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeLong(termId);
_data.writeLong(cntRemoved);
mRemote.transact(Stub.TRANSACTION_removeCompleted, _data, null, android.os.IBinder.FLAG_ONEWAY);
}
finally {
_data.recycle();
}
}
}
static final int TRANSACTION_lookupCompleted = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
static final int TRANSACTION_addCompleted = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
static final int TRANSACTION_removeCompleted = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
}
public void lookupCompleted(java.lang.String entry, x40241.holly.vo.a5.model.Term term) throws android.os.RemoteException;
public void addCompleted(java.lang.String entry, long termId) throws android.os.RemoteException;
public void removeCompleted(long termId, long cntRemoved) throws android.os.RemoteException;
}

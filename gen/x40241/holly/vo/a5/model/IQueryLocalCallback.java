/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: C:\\Users\\hvo\\Dropbox\\EDU\\COMSCI\\Android\\Android_II\\Assignment\\A5-Holly.Vo\\src\\x40241\\holly\\vo\\a5\\model\\IQueryLocalCallback.aidl
 */
package x40241.holly.vo.a5.model;
public interface IQueryLocalCallback extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements x40241.holly.vo.a5.model.IQueryLocalCallback
{
private static final java.lang.String DESCRIPTOR = "x40241.holly.vo.a5.model.IQueryLocalCallback";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an x40241.holly.vo.a5.model.IQueryLocalCallback interface,
 * generating a proxy if needed.
 */
public static x40241.holly.vo.a5.model.IQueryLocalCallback asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof x40241.holly.vo.a5.model.IQueryLocalCallback))) {
return ((x40241.holly.vo.a5.model.IQueryLocalCallback)iin);
}
return new x40241.holly.vo.a5.model.IQueryLocalCallback.Stub.Proxy(obj);
}
@Override public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_queryLocalCompleted:
{
data.enforceInterface(DESCRIPTOR);
java.util.List<x40241.holly.vo.a5.model.Term> _arg0;
_arg0 = data.createTypedArrayList(x40241.holly.vo.a5.model.Term.CREATOR);
this.queryLocalCompleted(_arg0);
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements x40241.holly.vo.a5.model.IQueryLocalCallback
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
@Override public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
@Override public void queryLocalCompleted(java.util.List<x40241.holly.vo.a5.model.Term> terms) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeTypedList(terms);
mRemote.transact(Stub.TRANSACTION_queryLocalCompleted, _data, null, android.os.IBinder.FLAG_ONEWAY);
}
finally {
_data.recycle();
}
}
}
static final int TRANSACTION_queryLocalCompleted = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
}
public void queryLocalCompleted(java.util.List<x40241.holly.vo.a5.model.Term> terms) throws android.os.RemoteException;
}

/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: C:\\Users\\hvo\\Dropbox\\EDU\\COMSCI\\Android\\Android_II\\Assignment\\A5-Holly.Vo\\src\\x40241\\holly\\vo\\a5\\model\\IDictService.aidl
 */
package x40241.holly.vo.a5.model;
public interface IDictService extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements x40241.holly.vo.a5.model.IDictService
{
private static final java.lang.String DESCRIPTOR = "x40241.holly.vo.a5.model.IDictService";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an x40241.holly.vo.a5.model.IDictService interface,
 * generating a proxy if needed.
 */
public static x40241.holly.vo.a5.model.IDictService asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof x40241.holly.vo.a5.model.IDictService))) {
return ((x40241.holly.vo.a5.model.IDictService)iin);
}
return new x40241.holly.vo.a5.model.IDictService.Stub.Proxy(obj);
}
@Override public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_lookupEntry:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
x40241.holly.vo.a5.model.IDictCallback _arg1;
_arg1 = x40241.holly.vo.a5.model.IDictCallback.Stub.asInterface(data.readStrongBinder());
boolean _result = this.lookupEntry(_arg0, _arg1);
reply.writeNoException();
reply.writeInt(((_result)?(1):(0)));
return true;
}
case TRANSACTION_removeEntry:
{
data.enforceInterface(DESCRIPTOR);
long _arg0;
_arg0 = data.readLong();
x40241.holly.vo.a5.model.IDictCallback _arg1;
_arg1 = x40241.holly.vo.a5.model.IDictCallback.Stub.asInterface(data.readStrongBinder());
boolean _result = this.removeEntry(_arg0, _arg1);
reply.writeNoException();
reply.writeInt(((_result)?(1):(0)));
return true;
}
case TRANSACTION_saveEntry:
{
data.enforceInterface(DESCRIPTOR);
x40241.holly.vo.a5.model.Term _arg0;
if ((0!=data.readInt())) {
_arg0 = x40241.holly.vo.a5.model.Term.CREATOR.createFromParcel(data);
}
else {
_arg0 = null;
}
x40241.holly.vo.a5.model.IDictCallback _arg1;
_arg1 = x40241.holly.vo.a5.model.IDictCallback.Stub.asInterface(data.readStrongBinder());
boolean _result = this.saveEntry(_arg0, _arg1);
reply.writeNoException();
reply.writeInt(((_result)?(1):(0)));
return true;
}
case TRANSACTION_queryLocal:
{
data.enforceInterface(DESCRIPTOR);
x40241.holly.vo.a5.model.IQueryLocalCallback _arg0;
_arg0 = x40241.holly.vo.a5.model.IQueryLocalCallback.Stub.asInterface(data.readStrongBinder());
boolean _result = this.queryLocal(_arg0);
reply.writeNoException();
reply.writeInt(((_result)?(1):(0)));
return true;
}
case TRANSACTION_queryLocalTerm:
{
data.enforceInterface(DESCRIPTOR);
long _arg0;
_arg0 = data.readLong();
x40241.holly.vo.a5.model.IQueryTermCallback _arg1;
_arg1 = x40241.holly.vo.a5.model.IQueryTermCallback.Stub.asInterface(data.readStrongBinder());
boolean _result = this.queryLocalTerm(_arg0, _arg1);
reply.writeNoException();
reply.writeInt(((_result)?(1):(0)));
return true;
}
case TRANSACTION_getPid:
{
data.enforceInterface(DESCRIPTOR);
long _result = this.getPid();
reply.writeNoException();
reply.writeLong(_result);
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements x40241.holly.vo.a5.model.IDictService
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
@Override public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
@Override public boolean lookupEntry(java.lang.String entry, x40241.holly.vo.a5.model.IDictCallback cb) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
boolean _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(entry);
_data.writeStrongBinder((((cb!=null))?(cb.asBinder()):(null)));
mRemote.transact(Stub.TRANSACTION_lookupEntry, _data, _reply, 0);
_reply.readException();
_result = (0!=_reply.readInt());
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
@Override public boolean removeEntry(long id, x40241.holly.vo.a5.model.IDictCallback cb) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
boolean _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeLong(id);
_data.writeStrongBinder((((cb!=null))?(cb.asBinder()):(null)));
mRemote.transact(Stub.TRANSACTION_removeEntry, _data, _reply, 0);
_reply.readException();
_result = (0!=_reply.readInt());
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
@Override public boolean saveEntry(x40241.holly.vo.a5.model.Term term, x40241.holly.vo.a5.model.IDictCallback cb) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
boolean _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
if ((term!=null)) {
_data.writeInt(1);
term.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
_data.writeStrongBinder((((cb!=null))?(cb.asBinder()):(null)));
mRemote.transact(Stub.TRANSACTION_saveEntry, _data, _reply, 0);
_reply.readException();
_result = (0!=_reply.readInt());
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
@Override public boolean queryLocal(x40241.holly.vo.a5.model.IQueryLocalCallback cb) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
boolean _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeStrongBinder((((cb!=null))?(cb.asBinder()):(null)));
mRemote.transact(Stub.TRANSACTION_queryLocal, _data, _reply, 0);
_reply.readException();
_result = (0!=_reply.readInt());
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
@Override public boolean queryLocalTerm(long id, x40241.holly.vo.a5.model.IQueryTermCallback cb) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
boolean _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeLong(id);
_data.writeStrongBinder((((cb!=null))?(cb.asBinder()):(null)));
mRemote.transact(Stub.TRANSACTION_queryLocalTerm, _data, _reply, 0);
_reply.readException();
_result = (0!=_reply.readInt());
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
@Override public long getPid() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
long _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_getPid, _data, _reply, 0);
_reply.readException();
_result = _reply.readLong();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
}
static final int TRANSACTION_lookupEntry = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
static final int TRANSACTION_removeEntry = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
static final int TRANSACTION_saveEntry = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
static final int TRANSACTION_queryLocal = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
static final int TRANSACTION_queryLocalTerm = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
static final int TRANSACTION_getPid = (android.os.IBinder.FIRST_CALL_TRANSACTION + 5);
}
public boolean lookupEntry(java.lang.String entry, x40241.holly.vo.a5.model.IDictCallback cb) throws android.os.RemoteException;
public boolean removeEntry(long id, x40241.holly.vo.a5.model.IDictCallback cb) throws android.os.RemoteException;
public boolean saveEntry(x40241.holly.vo.a5.model.Term term, x40241.holly.vo.a5.model.IDictCallback cb) throws android.os.RemoteException;
public boolean queryLocal(x40241.holly.vo.a5.model.IQueryLocalCallback cb) throws android.os.RemoteException;
public boolean queryLocalTerm(long id, x40241.holly.vo.a5.model.IQueryTermCallback cb) throws android.os.RemoteException;
public long getPid() throws android.os.RemoteException;
}

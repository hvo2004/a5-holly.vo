/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: C:\\Users\\hvo\\Dropbox\\EDU\\COMSCI\\Android\\Android_II\\Assignment\\A5-Holly.Vo\\src\\x40241\\holly\\vo\\a5\\model\\IServiceReadyCallback.aidl
 */
package x40241.holly.vo.a5.model;
public interface IServiceReadyCallback extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements x40241.holly.vo.a5.model.IServiceReadyCallback
{
private static final java.lang.String DESCRIPTOR = "x40241.holly.vo.a5.model.IServiceReadyCallback";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an x40241.holly.vo.a5.model.IServiceReadyCallback interface,
 * generating a proxy if needed.
 */
public static x40241.holly.vo.a5.model.IServiceReadyCallback asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof x40241.holly.vo.a5.model.IServiceReadyCallback))) {
return ((x40241.holly.vo.a5.model.IServiceReadyCallback)iin);
}
return new x40241.holly.vo.a5.model.IServiceReadyCallback.Stub.Proxy(obj);
}
@Override public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_isBounded:
{
data.enforceInterface(DESCRIPTOR);
this.isBounded();
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements x40241.holly.vo.a5.model.IServiceReadyCallback
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
@Override public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
@Override public void isBounded() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_isBounded, _data, null, android.os.IBinder.FLAG_ONEWAY);
}
finally {
_data.recycle();
}
}
}
static final int TRANSACTION_isBounded = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
}
public void isBounded() throws android.os.RemoteException;
}
